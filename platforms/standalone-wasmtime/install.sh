#!/bin/bash

cd $(dirname $0)

rm -rf ./wasmtime
echo 'Installing wasmtime'
export WASMTIME_HOME="./wasmtime" 
curl https://wasmtime.dev/install.sh -sSf | bash
