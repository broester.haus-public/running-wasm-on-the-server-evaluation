#!/bin/bash

# $1 PATH to wasm module
# $2 Fibonacci size

platforms/standalone-wasmtime/wasmtime/bin/wasmtime run $1 -- $2 | ts %Y-%m-%dT%H:%M:%.SZ