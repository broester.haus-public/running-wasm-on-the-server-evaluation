#!/bin/bash

cd $(dirname $0)

echo "Installing kind"
curl -Lo ./kind https://kind.sigs.k8s.io/dl/v0.14.0/kind-linux-amd64
chmod +x ./kind
sudo mv -f ./kind /usr/bin/kind


echo "Installing kubectl"
curl -LO "https://dl.k8s.io/release/v1.24.0/bin/linux/amd64/kubectl"
install -o root -g root -m 0755 kubectl /usr/local/bin/kubectl
rm kubectl


echo "Verfiying installation of kind and kubectl"
kind --version
kubectl version --client --short


echo "Installing wasm-to-oci"
curl -L https://github.com/engineerd/wasm-to-oci/releases/download/v0.1.2/linux-amd64-wasm-to-oci -o wasm-to-oci
chmod +x wasm-to-oci
sudo mv -f wasm-to-oci /usr/local/bin


echo "Installing krustlet"
mkdir ./krustlet-install && cd ./krustlet-install
curl -o krustlet.tar.gz https://krustlet.blob.core.windows.net/releases/krustlet-v0.7.0-linux-amd64.tar.gz 
tar -xzf krustlet.tar.gz
sudo mv krustlet-wasi /usr/local/bin/
cd ../ && rm -rf ./krustlet-install
