#!/bin/bash

kubectl get pods -o name | xargs kubectl logs --timestamps | grep "#Startup_complete" | cut -d' ' -f2