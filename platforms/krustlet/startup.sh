#!/bin/bash

cd $(dirname $0)

echo "Creating cluster in kind"
sudo kind create cluster --config=kind-cluster-config.yaml
kind export kubeconfig
kubectl apply -f https://projectcontour.io/quickstart/contour.yaml
kubectl patch daemonsets -n projectcontour envoy -p '{"spec":{"template":{"spec":{"nodeSelector":{"ingress-ready":"true"},"tolerations":[{"key":"node-role.kubernetes.io/master","operator":"Equal","effect":"NoSchedule"}]}}}}'


echo "Bootstrapping krustlet"
mkdir ./krustlet-install && cd ./krustlet-install
curl -o ./bootstrap.sh https://raw.githubusercontent.com/krustlet/krustlet/main/scripts/bootstrap.sh 
chmod +x bootstrap.sh
bash ./bootstrap.sh
cd .. && rm -rf ./krustlet-install


echo "Starting krustlet"
HOSTNAME=$(hostname)
kubectl delete csr "$HOSTNAME-tls"
sudo krustlet-wasi --node-ip 172.17.0.1 --bootstrap-file $HOME/.krustlet/config/bootstrap.conf --insecure-registries localhost:5000 &
echo "Waiting for krustlet to come up"
sleep 15
kubectl certificate approve "$HOSTNAME-tls"