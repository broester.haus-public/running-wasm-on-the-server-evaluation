#!/bin/bash

# $1 location of resource yaml
# $2 fibonacci size
# $3 job name

cat $1 | sed "s/%SIZE/$2/"| kubectl apply --force -f -
kubectl wait --for=condition=complete job.batch/$3 --timeout=60s
