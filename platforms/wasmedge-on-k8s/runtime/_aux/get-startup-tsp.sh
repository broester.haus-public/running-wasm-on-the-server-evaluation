#!/bin/bash

export KUBECONFIG=/var/run/kubernetes/admin.kubeconfig 
kubectl get pods -o name | xargs kubectl logs --timestamps | grep "#Startup_complete" | cut -d' ' -f1