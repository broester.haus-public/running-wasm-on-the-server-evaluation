#!/bin/bash

# $1 location of resource yaml

export KUBECONFIG=/var/run/kubernetes/admin.kubeconfig 
cat $1 | kubectl apply --force -f -