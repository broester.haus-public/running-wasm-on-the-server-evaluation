#!/bin/bash

cd $(dirname $0)
sudo systemctl restart crio
sudo -b CGROUP_DRIVER=systemd CONTAINER_RUNTIME=remote CONTAINER_RUNTIME_ENDPOINT='unix:///var/run/crio/crio.sock' ../wasmedge/kubernetes/hack/local-up-cluster.sh 