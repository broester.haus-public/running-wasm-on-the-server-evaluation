#!/bin/bash

# $1 location of resource yaml
# $2 fibonacci size
# $3 job name

export KUBECONFIG=/var/run/kubernetes/admin.kubeconfig 
cat $1 | sed "s/%SIZE/$2/" | kubectl apply --force -f -
kubectl wait --for=condition=complete job.batch/$3 --timeout=180s
