#!/bin/bash

cd $(dirname $0)
bash ../../workloads/source/fib_recursive/wasmedge/build.sh
bash ../../workloads/source/fib_iterative/wasmedge/build.sh
bash ../../workloads/source/http/wasmedge/http_server/build.sh