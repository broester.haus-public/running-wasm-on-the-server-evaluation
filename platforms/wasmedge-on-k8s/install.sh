#!/bin/bash

cd $(dirname $0)

rm -rf ./wasmedge
mkdir ./wasmedge
chmod a+w ./wasmedge
cd ./wasmedge

echo '######### Installing CRI-O #########'
../install-crio.sh

echo '######### Installing Kubernetes #########'
../install-k8s.sh

echo '######### Installing Buildah #########'
../install-buildah.sh

