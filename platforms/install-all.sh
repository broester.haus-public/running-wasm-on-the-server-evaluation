#!/bin/bash

cd $(dirname $0)

echo 'Installing required utils'
sudo apt install moreutils
cargo install cargo-wasi

echo 'Installing wasmEdge on K8s'
./wasmedge-on-k8s/install.sh

echo 'Installing wasmCloud'
./wasmCloud/install.sh

echo 'Installing vino'
./vino/install.sh

echo 'Installing standalone wasmer'
./standalone-wasmer/install.sh

echo 'Installing standalone wasmtime'
./standalone-wasmtime/install.sh

echo 'Installing krustlet'
./krustlet/install.sh

echo 'Starting local oci registry'
docker run -d -p 5000:5000 --restart=always --name registry registry:2

echo 'Building containered wasmer images'
./containered-wasmer/build.sh

echo 'Building containered wasmtime images'
./containered-wasmtime/build.sh

echo 'Pushing wasm images to local oci registy'
./wasmedge-on-k8s/build.sh
./wasmCloud/build.sh
./krustlet/build.sh

echo 'Adding necessary directories'
mkdir -p ../logs/wasmcloud
mkdir -p ../logs/vino
mkdir -p ../result