#!/bin/bash

cd $(dirname $0)

rm -rf ./wasmer
echo 'Installing wasmer'
export WASMER_DIR="./wasmer" 
curl https://get.wasmer.io -sSfL | bash
