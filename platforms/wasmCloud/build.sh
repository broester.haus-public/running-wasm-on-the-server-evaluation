#!/bin/bash

cd $(dirname $0)
bash ../../workloads/source/fib_recursive/wasmcloud-actor/fib/build.sh
bash ../../workloads/source/fib_iterative/wasmcloud-actor/fibi/build.sh
bash ../../workloads/source/http/wasmcloud-actor/http-echo/build.sh