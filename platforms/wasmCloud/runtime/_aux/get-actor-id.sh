#!/bin/bash

# $1 Host Id

wash ctl get inventory $1 --output json | jq -r .inventory.actors[0].id