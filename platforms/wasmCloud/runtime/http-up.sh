#!/bin/bash

# $1 host id

cd $(dirname $0)
wash ctl start actor --host-id $1 localhost:5000/wasmcloud-http-echo:1
wash ctl start provider wasmcloud.azurecr.io/httpserver:0.14.4
echo "Waiting 10 sec for provider startup"
sleep 10

PROVIDERID=$(_aux/get-provider-id.sh $1)
echo "Provider ID: $PROVIDERID"
ACTORID=$(_aux/get-actor-id.sh $1)

wash ctl link put $ACTORID $PROVIDERID wasmcloud:httpserver address=0.0.0.0:22998

