#!/bin/bash

# $1 Cluster seed
# $2 Actor id
# $3 Fibonacci size

### Do not be fooled. Even if were invoking this like a http server, no http stack is used, because this circumvents the http-server capability and calls the function directly

wash call -t 360000 -c $1 $2 HttpServer.HandleRequest '{"method": "GET", "path": "/", "body": "", "queryString":"n='"$3"'", "header":{}}'