#!/bin/bash

cd $(dirname $0)
> ../../../logs/wasmcloud/log.txt
../wasmcloud/nats/nats-server --jetstream > /dev/null &
WASMCLOUD_OCI_ALLOWED_INSECURE=localhost:5000 WASMCLOUD_RPC_TIMEOUT_MS=360000 WASMCLOUD_PROV_RPC_TIMEOUT_MS=360000 ../wasmcloud/wasmcloud-otp/bin/wasmcloud_host foreground >> ../../../logs/wasmcloud/log.txt &