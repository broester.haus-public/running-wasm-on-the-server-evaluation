#!/bin/bash

# $1 host id

cd $(dirname $0)
PROVIDERID=$(_aux/get-provider-id.sh $1)
ACTORID=$(_aux/get-actor-id.sh $1)

wash ctl link del $ACTORID wasmcloud:httpserver 
wash ctl stop provider $1 $PROVIDERID default wasmcloud:httpserver
_aux/post-run.sh $1 $ACTORID


