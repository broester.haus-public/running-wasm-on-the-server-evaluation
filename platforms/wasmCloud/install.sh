#!/bin/bash

cd $(dirname $0)

mkdir ./tmp
chmod a+w ./tmp
cd ./tmp

rm -rf ../wasmcloud

echo 'Installing rust'
curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs | bash -s -- -y

echo 'Installing wash'
curl -s https://packagecloud.io/install/repositories/wasmcloud/core/script.deb.sh | bash
apt install wash

echo 'Installing NATS'
curl -L https://github.com/nats-io/nats-server/releases/download/v2.8.4/nats-server-v2.8.4-linux-amd64.tar.gz -o nats-server
mkdir -p ../wasmcloud/nats
tar xzf nats-server 
mv nats-server-v2.8.4-linux-amd64/nats-server ../wasmcloud/nats/


echo 'Installing wasmCloud OTP Server'
curl -L https://github.com/wasmCloud/wasmcloud-otp/releases/download/v0.54.6/x86_64-linux.tar.gz -o wasmcloud-otp
mkdir -p ../wasmcloud/wasmcloud-otp
tar xzf wasmcloud-otp -C ../wasmcloud/wasmcloud-otp

chmod -R 777 ../wasmcloud/

echo 'cleanup'
cd ..
rm -rf ./tmp