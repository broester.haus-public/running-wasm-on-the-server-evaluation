#!/bin/bash

# $1 PATH to wasm module
# $2 Fibonacci size

platforms/vino/vino/vow run $1 cmp --log-json --log-dir ./logs/vino --debug -- --input=$2  