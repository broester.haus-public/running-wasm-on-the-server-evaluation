#!/bin/bash

cd $(dirname $0)
rm -rf ./vino

echo 'Installing vino'
export INSTALL_DIR="./vino" 
curl -fsSL https://releases.vino.dev/install.sh | /bin/bash