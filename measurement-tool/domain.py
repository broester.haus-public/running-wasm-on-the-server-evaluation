from enumdefs import Platform, WorkloadType, WorkloadSize


class MeasurementSpec:
    def __repr__(self):
        return f"MeasurementSpec(platform: {self.platform.name}, workload_type: {self.workload_type.name}, workload_size: {self.workload_size.name})"

    def __init__(self, platform: Platform, workload_type: WorkloadType, workload_size: WorkloadSize):
        self.platform = platform
        self.workload_type = workload_type
        self.workload_size = workload_size


class MeasurementRun:
    def __repr__(self):
        return f"MeasurementRun(runId: {self.run_id}, repId: {self.rep_id}, spec: {repr(self.measurement_spec)})"

    def __init__(self, run_id: int, rep_id: int, measurement_spec: MeasurementSpec):
        self.measurement_spec = measurement_spec
        self.run_id = run_id
        self.rep_id = rep_id


class ResultMetrics:
    def __init__(self, startup_time_us: int | None = None, completion_time_us: int | None = None,
                 stop_time_us: int | None = None, http_metrics=None):
        self.stop_time_us = stop_time_us
        self.startup_time_us = startup_time_us
        self.completion_time_us = completion_time_us

        self.http_metrics = http_metrics

        if completion_time_us is not None:
            if startup_time_us is not None:
                running_time_us = completion_time_us - startup_time_us
            else:
                running_time_us = completion_time_us
        else:
            running_time_us = None

        self.running_time_us = running_time_us


class MeasurementRunResult:
    def __init__(self, measurement_run: MeasurementRun, result_metrics: ResultMetrics):
        self.result_metrics = result_metrics
        self.measurement_run = measurement_run
