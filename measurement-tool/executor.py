from typing import List
import time
import math

from domain import *
from config import Config, PlatformConfig, Env
from metrics_writer import MetricsWriter
from platform_executors.abstract_executor import AbstractExecutor
from platform_executors.cli_wasmer_wasmtime_executor import CliWasmerWasmtimeExecutor
from platform_executors.containered_wasmer_wasmtime_executor import ContaineredWasmerWasmtimeExecutor
from platform_executors.k8s_wasmer_wasmtime_executor import K8sWasmerWasmtimeExecutor
from platform_executors.krustlet_executor import KrustletExecutor
from platform_executors.vino_executor import VinoExecutor
from platform_executors.wasmcloud_executor import WasmcloudExecutor
from platform_executors.wasmedge_executor import WasmedgeExecutor


class RunManager:
    run_id = 0


def execute_requested_measurements(config: Config):
    metrics_writer = MetricsWriter()
    metrics_writer.open_and_init("result/Metrics_" + str(math.floor(time.time())) + ".csv")

    for platform_str in config.measurement_requests.platforms:
        platform = Platform[platform_str]
        platform_executor = get_platform_executor(platform, config.env)

        startup_platform(platform, platform_executor)

        for workload_type_str in config.measurement_requests.workload_types:
            workload_type = WorkloadType[workload_type_str]

            if workload_type.name not in get_platform_config(config.env.platforms, platform).supported_workload_types:
                print("Workload type " + repr(workload_type) + " is not supported for platform " + repr(platform))
                continue

            if workload_type == WorkloadType.HTTP:
                platform_executor.open_http()

            for workload_size_str in config.measurement_requests.workload_sizes:
                workload_size = WorkloadSize[workload_size_str]

                spec = MeasurementSpec(platform, workload_type, workload_size)

                run_metrics = execute_runs_for_spec(spec, config, platform_executor)

                for run_metric in run_metrics:
                    metrics_writer.write_entry(run_metric)

            if workload_type == WorkloadType.HTTP:
                platform_executor.close_http()

        shutdown_platform(platform_executor)

    metrics_writer.close()


def execute_runs_for_spec(spec, config, executor: AbstractExecutor) -> List[MeasurementRunResult]:
    number_of_warmup_runs = config.measurement_requests.warmup_runs_compute \
        if spec.workload_type != WorkloadType.HTTP \
        else config.measurement_requests.warmup_runs_http

    number_of_measured_runs = config.measurement_requests.measured_runs_compute \
        if spec.workload_type != WorkloadType.HTTP \
        else config.measurement_requests.measured_runs_http

    print("####### Starting runs for spec: " + repr(spec))
    print("##### Warming up with " + repr(number_of_warmup_runs) + " runs")
    for i in range(number_of_warmup_runs):
        execute_single_run(MeasurementRun(0, i, spec), executor)

    print("##### Executing " + repr(number_of_measured_runs) + " measured runs")
    result_list = []
    for i in range(number_of_measured_runs):
        RunManager.run_id += 1
        result = execute_single_run(MeasurementRun(RunManager.run_id, i, spec), executor)
        result_list.append(result)

    return result_list


def get_platform_executor(platform: Platform, env: Env) -> AbstractExecutor:
    matched_config = get_platform_config(env.platforms, platform)

    match platform:
        case Platform.VINO:
            return VinoExecutor(matched_config, env)
        case Platform.KRUSTLET:
            return KrustletExecutor(matched_config, env)
        case Platform.WASMCLOUD:
            return WasmcloudExecutor(matched_config, env)
        case Platform.WASMEDGE_ON_K8S:
            return WasmedgeExecutor(matched_config, env)
        case Platform.CLI_WASMER | Platform.CLI_WASMTIME:
            return CliWasmerWasmtimeExecutor(matched_config, env)
        case Platform.CONTAINERED_WASMER | Platform.CONTAINERED_WASMTIME:
            return ContaineredWasmerWasmtimeExecutor(matched_config, env)
        case Platform.K8S_WASMER | Platform.K8S_WASMTIME:
            return K8sWasmerWasmtimeExecutor(matched_config, env)
        case _:
            raise Exception("Unknown platform: " + str(platform))


def get_platform_config(platform_configs, platform) -> PlatformConfig:
    return next(filter(lambda s: s.name == platform.name, platform_configs))


def startup_platform(platform, platform_executor):
    print("######### Starting platform " + str(platform))
    process = platform_executor.startup()
    if process is not None and process.returncode != 0:
        print(process.stdout)
        print(process.stderr)
        raise Exception("Error while starting platform " + str(platform))


def shutdown_platform(platform_executor: AbstractExecutor):
    print("######### Shutting down platform")
    platform_executor.shutdown()


def execute_single_run(run: MeasurementRun, executor: AbstractExecutor) -> MeasurementRunResult:
    print("### Executing run: " + str(run))
    try:
        return executor.run(run)
    except Exception as e:
        print("Exception while executing run: " + repr(e))
        return MeasurementRunResult(run, ResultMetrics(None, None, None, None))
