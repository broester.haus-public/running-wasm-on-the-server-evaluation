import config
import executor

if __name__ == '__main__':
    config = config.read_config_from_yaml("config.yaml")
    executor.execute_requested_measurements(config)
