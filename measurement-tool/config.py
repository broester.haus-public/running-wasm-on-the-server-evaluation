from typing import List

import yaml
import munch

class MeasurementRequest:
    def __init__(self, platforms: List[str], workload_types: List[str], workload_sizes: List[str], warmup_runs_compute: int,
                 measured_runs_compute: int, measured_runs_http: int, warmup_runs_http: int):
        self.platforms = platforms
        self.workload_types = workload_types
        self.workload_sizes = workload_sizes
        self.warmup_runs_compute = warmup_runs_compute
        self.measured_runs_compute = measured_runs_compute
        self.warmup_runs_http = warmup_runs_http
        self.measured_runs_http = measured_runs_http


class PlatformConfig:
    def __init__(self, name, startup_script, run_script, shutdown_script, http_up_script, http_down_script, auxiliary_script_path, supported_workload_types: List[str], http_invocation_path: str, http_invocation_port: int, http_payload: str):
        self.http_payload = http_payload
        self.http_invocation_port = http_invocation_port
        self.http_invocation_path = http_invocation_path
        self.auxiliary_script_path = auxiliary_script_path
        self.http_up_script = http_up_script
        self.http_down_script = http_down_script
        self.name = name
        self.startup_script = startup_script
        self.run_script = run_script
        self.shutdown_script = shutdown_script
        self.supported_workload_types = supported_workload_types


class Env:
    def __init__(self, platforms: List[PlatformConfig], wasm_module_base_path: str, log_path: str, load_server_url: str, invocation_host: str):
        self.invocation_host = invocation_host
        self.load_server_url = load_server_url
        self.log_path = log_path
        self.wasm_module_base_path = wasm_module_base_path
        self.platforms = platforms


class Config:
    def __init__(self, measurement_requests: MeasurementRequest, env: Env):
        self.measurement_requests = measurement_requests
        self.env = env


def read_config_from_yaml(path: str) -> Config:
    with open(path) as stream:
        try:
            config_dict = yaml.safe_load(stream)
            return munch.munchify(config_dict)
        except yaml.YAMLError as exc:
            print(exc)
