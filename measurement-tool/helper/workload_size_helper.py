from enumdefs import WorkloadSize, WorkloadType


def get_fibonacci_n(workload_type: WorkloadType, workload_size: WorkloadSize) -> int:
    if workload_type == WorkloadType.FIB_ITERATIVE:
        if workload_size == WorkloadSize.SMALL:
            return 20
        elif workload_size == WorkloadSize.MEDIUM:
            return 40000
        else:
            return 80000000

    if workload_size == WorkloadSize.SMALL:
        return 20
    elif workload_size == WorkloadSize.MEDIUM:
        return 40
    else:
        return 45


def get_http_qps(workload_size: WorkloadSize) -> int:
    if workload_size == WorkloadSize.SMALL:
        return 250
    elif workload_size == WorkloadSize.MEDIUM:
        return 500
    else:
        return 1000


def get_http_c(workload_size: WorkloadSize) -> int:
    if workload_size == WorkloadSize.SMALL:
        return 32
    elif workload_size == WorkloadSize.MEDIUM:
        return 64
    else:
        return 128
