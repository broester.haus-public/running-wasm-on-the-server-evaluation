import requests

from config import PlatformConfig, Env
from enumdefs import WorkloadSize
from helper import workload_size_helper


def start_fortio_load(platform_config: PlatformConfig, env: Env, workload_size: WorkloadSize) -> str:
    invocation_url = env.invocation_host + ":" + str(
        platform_config.http_invocation_port) + platform_config.http_invocation_path
    fortio_url = "http://" + env.load_server_url + "/fortio/rest/run?jsonPath=.metadata"

    json = {
        "metadata": {
            "url": invocation_url,
            "payload": platform_config.http_payload,
            "qps": str(workload_size_helper.get_http_qps(workload_size)),
            "c": str(workload_size_helper.get_http_c(workload_size)),
            "t": "20s",
            "p": "10, 20, 25, 30, 40, 50, 60, 70, 75, 80, 90, 95, 99, 99.9",
            "headers": [
                "Content-Type: application/json"
            ],
            "save": "on"
        }
    }
    response = requests.post(url=fortio_url, json=json)

    return response.json()
