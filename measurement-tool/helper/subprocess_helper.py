import subprocess


def execute_subprocess(process_str, timeout=30) -> subprocess.CompletedProcess:
    process = subprocess.run(process_str, capture_output=True, timeout=timeout)
    return process


def execute_subprocess_unwatched(process_str, timeout=30) -> subprocess.CompletedProcess:
    return subprocess.run(process_str, timeout=timeout)


def popen_subprocess_non_blocking(process_str) -> subprocess.Popen:
    return subprocess.Popen(process_str)
