from enum import Enum


class Platform(Enum):
    KRUSTLET = 1
    VINO = 2
    WASMCLOUD = 3
    CLI_WASMER = 4
    CLI_WASMTIME = 5
    CONTAINERED_WASMER = 6
    CONTAINERED_WASMTIME = 7
    K8S_WASMER = 8
    K8S_WASMTIME = 9
    WASMEDGE_ON_K8S = 10


class WorkloadType(Enum):
    FIB_RECURSIVE = 0
    FIB_ITERATIVE = 1
    HTTP = 2


class WorkloadSize(Enum):
    SMALL = 1
    MEDIUM = 2
    LARGE = 3
