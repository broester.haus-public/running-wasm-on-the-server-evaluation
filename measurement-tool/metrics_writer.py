import csv

from domain import MeasurementRunResult


class MetricsWriter:
    fieldnames = [
        "run_id",
        "platform",
        "workload_type",
        "workload_size",
        "rep_id",
        "startup_time_us",
        "completion_time_us",
        "running_time_us",
        "stop_time_us",
        "http_metrics"]

    def __init__(self):
        self.file = None
        self.internal_writer: csv.DictWriter | None = None

    def open_and_init(self, filename: str):
        self.file = open(filename, mode='w')

        self.internal_writer = csv.DictWriter(self.file, fieldnames=MetricsWriter.fieldnames)
        self.internal_writer.writeheader()

    def write_entry(self, measurement_run_result: MeasurementRunResult):
        entry_dict = {
            "run_id": measurement_run_result.measurement_run.run_id,
            "platform": measurement_run_result.measurement_run.measurement_spec.platform.name,
            "workload_type": measurement_run_result.measurement_run.measurement_spec.workload_type.name,
            "workload_size": measurement_run_result.measurement_run.measurement_spec.workload_size.name,
            "rep_id": measurement_run_result.measurement_run.rep_id,
            "startup_time_us": measurement_run_result.result_metrics.startup_time_us,
            "completion_time_us": measurement_run_result.result_metrics.completion_time_us,
            "running_time_us": measurement_run_result.result_metrics.running_time_us,
            "stop_time_us": measurement_run_result.result_metrics.stop_time_us,
            "http_metrics": measurement_run_result.result_metrics.http_metrics
        }
        self.internal_writer.writerow(entry_dict)
        self.file.flush()

    def close(self):
        self.file.close()
