import datetime
import math
import time

from domain import *
from helper import subprocess_helper
from platform_executors.abstract_executor import AbstractExecutor


class KrustletExecutor(AbstractExecutor):

    def run_non_http(self, measurement_run: MeasurementRun) -> MeasurementRunResult:
        path_to_resource_def = self.get_file_in_workload_dir(measurement_run.measurement_spec.platform,
                                                             measurement_run.measurement_spec.workload_type,
                                                             "wasm-job.yaml")
        fibonacci_n = self.get_fibonacci_n(measurement_run)
        job_name = self.get_job_name(measurement_run)

        startup_before = datetime.datetime.now(datetime.timezone.utc)
        ns_before = time.perf_counter_ns()
        subprocess_helper.execute_subprocess_unwatched([self.platform_config.run_script, path_to_resource_def,
                                                        fibonacci_n, job_name])

        ns_after = time.perf_counter_ns()

        startup_time_us = self.get_startup_time(startup_before)
        completion_time_us = math.floor((ns_after - ns_before) / 1000)

        subprocess_helper.execute_subprocess_unwatched([self.platform_config.auxiliary_script_path + "/delete-jobs.sh"])

        metrics = ResultMetrics(startup_time_us=startup_time_us, completion_time_us=completion_time_us)

        return MeasurementRunResult(measurement_run, metrics)

    def get_job_name(self, measurement_run):
        return "fib-iterative" if measurement_run.measurement_spec.workload_type == WorkloadType.FIB_ITERATIVE \
            else "fib-recursive"

    def get_startup_time(self, startup_before):
        startup_str = subprocess_helper.execute_subprocess(
            [self.platform_config.auxiliary_script_path + "/get-startup-tsp.sh"]).stdout.decode(
            'utf-8')  # cut out timestamp without timezone
        startup_utc_timestamp = int(startup_str[0:10])
        startup_millis = int(startup_str[11:14])
        tsp = datetime.datetime.utcfromtimestamp(startup_utc_timestamp) + datetime.timedelta(
            milliseconds=startup_millis)
        startup_time_diff = tsp.replace(tzinfo=datetime.timezone.utc) - startup_before
        startup_time_us = math.floor(startup_time_diff.total_seconds() * 1000 * 1000)  # from seconds to microseconds
        return startup_time_us

    def startup(self) -> None:
        subprocess_helper.execute_subprocess_unwatched([self.platform_config.startup_script], timeout=120)

    def shutdown(self) -> None:
        subprocess_helper.execute_subprocess_unwatched([self.platform_config.shutdown_script])
