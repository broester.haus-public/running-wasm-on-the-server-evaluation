import datetime
import math
import time

from domain import *
from helper import subprocess_helper
from platform_executors.abstract_executor import AbstractExecutor


class K8sWasmerWasmtimeExecutor(AbstractExecutor):

    def run_non_http(self, measurement_run: MeasurementRun) -> MeasurementRunResult:
        path_to_resource_def = self.get_file_in_workload_dir(measurement_run.measurement_spec.platform,
                                                             measurement_run.measurement_spec.workload_type,
                                                             "wasm-job.yaml")
        fibonacci_n = self.get_fibonacci_n(measurement_run)
        job_name = self.get_job_name(measurement_run)

        startup_before = datetime.datetime.now()
        ns_before = time.perf_counter_ns()
        subprocess_helper.execute_subprocess_unwatched([self.platform_config.run_script, path_to_resource_def,
                                                        fibonacci_n,
                                                        job_name], timeout=180)

        ns_after = time.perf_counter_ns()

        startup_time_us = self.get_startup_time(startup_before)
        completion_time_us = math.floor((ns_after - ns_before) / 1000)

        self.delete_old_jobs()

        metrics = ResultMetrics(startup_time_us=startup_time_us, completion_time_us=completion_time_us)

        return MeasurementRunResult(measurement_run, metrics)

    def get_job_name(self, measurement_run):
        return "wasmer-fib" if measurement_run.measurement_spec.platform == Platform.K8S_WASMER \
            else "wasmtime-fib"

    def get_startup_time(self, startup_before):
        startup_str = subprocess_helper.execute_subprocess(
            [self.platform_config.auxiliary_script_path + "/get-startup-tsp.sh"]).stdout.decode('utf-8')[
                      0:26]  # cut out timestamp without timezone
        tsp = datetime.datetime.strptime(startup_str, "%Y-%m-%dT%H:%M:%S.%f")
        startup_time_diff = tsp - startup_before
        startup_time_us = math.floor(startup_time_diff.total_seconds() * 1000 * 1000)  # from seconds to microseconds
        return startup_time_us

    def delete_old_jobs(self):
        subprocess_helper.execute_subprocess_unwatched([self.platform_config.auxiliary_script_path + "/delete-jobs.sh"])

    def is_up_rc(self):
        return subprocess_helper.execute_subprocess_unwatched(
            [self.platform_config.auxiliary_script_path + "/is-up.sh"]).returncode

    def startup(self) -> None:
        subprocess_helper.popen_subprocess_non_blocking([self.platform_config.startup_script])

        for i in range(120):
            rc = self.is_up_rc()
            print("Is up? RC: " + str(rc))
            if rc == 0:
                break
            time.sleep(10)
        print("Waiting for crun to be available")
        time.sleep(30)

    def shutdown(self) -> None:
        subprocess_helper.execute_subprocess_unwatched([self.platform_config.shutdown_script])
