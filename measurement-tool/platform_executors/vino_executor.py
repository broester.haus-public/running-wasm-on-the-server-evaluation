import datetime
import glob
import math
import os
import time
from subprocess import Popen

import jq as jq

from config import PlatformConfig, Env
from domain import *
from helper import subprocess_helper, requests_helper
from platform_executors.abstract_executor import AbstractExecutor


class VinoExecutor(AbstractExecutor):

    def __init__(self, platform_config: PlatformConfig, env: Env):
        super().__init__(platform_config, env)
        self.running_process: Popen | None = None

    def run_http(self, measurement_run: MeasurementRun) -> MeasurementRunResult:
        metrics = requests_helper.start_fortio_load(self.platform_config, self.env, measurement_run.measurement_spec.workload_size)
        return MeasurementRunResult(measurement_run, ResultMetrics(http_metrics=metrics))

    def run_non_http(self, measurement_run: MeasurementRun) -> MeasurementRunResult:
        path_to_module = self.get_path_to_module_by_measurement_run(measurement_run)
        fibonacci_n = self.get_fibonacci_n(measurement_run)

        startup_before_utc = datetime.datetime.now(datetime.timezone.utc)
        ns_before = time.perf_counter_ns()

        subprocess_helper.execute_subprocess([self.platform_config.run_script, path_to_module, fibonacci_n])

        ns_after = time.perf_counter_ns()

        completion_time_us = math.floor((ns_after - ns_before) / 1000)
        startup_time_us = self.get_startup_time(startup_before_utc)

        metrics = ResultMetrics(startup_time_us=startup_time_us, completion_time_us=completion_time_us)

        return MeasurementRunResult(measurement_run, metrics)

    def get_startup_time(self, startup_before_utc):
        list_of_files = glob.glob(self.env.log_path + '/vino/logs/*')
        latest_file = max(list_of_files, key=os.path.getctime)
        with (open(latest_file, mode="r")) as file:
            lines = file.readlines()
            for line in lines:

                if "wasm invoke" in jq.compile(".msg").input(text=line).first():
                    time_str = jq.compile(".time").input(text=line).first()
                    tsp = datetime.datetime.strptime(time_str[0:26] + "Z", "%Y-%m-%dT%H:%M:%S.%f%z")
                    startup_time_diff = tsp - startup_before_utc
                    startup_time_us = math.floor(
                        startup_time_diff.total_seconds() * 1000 * 1000)  # from seconds to microseconds
        return startup_time_us

    def open_http(self) -> None:
        path_to_module = self.get_path_to_module(Platform.VINO, WorkloadType.HTTP)
        self.running_process = subprocess_helper.popen_subprocess_non_blocking(
            [self.platform_config.http_up_script, path_to_module])
        pass

    def close_http(self) -> None:
        self.running_process.terminate()
        pass
