import datetime
import math
import time

from domain import *
from helper import subprocess_helper
from platform_executors.abstract_executor import AbstractExecutor


class ContaineredWasmerWasmtimeExecutor(AbstractExecutor):

    def run_non_http(self, measurement_run: MeasurementRun) -> MeasurementRunResult:
        module_name = self.get_module_name(measurement_run)
        fibonacci_n = self.get_fibonacci_n(measurement_run)

        startup_before_utc = datetime.datetime.now(datetime.timezone.utc)
        ns_before = time.perf_counter_ns()

        completed_process = subprocess_helper.execute_subprocess([self.platform_config.run_script, module_name,
                                                                  fibonacci_n])
        ns_after = time.perf_counter_ns()

        completion_time_us = math.floor((ns_after - ns_before) / 1000)
        startup_time_us = self.get_startup_time(completed_process, startup_before_utc)

        metrics = ResultMetrics(startup_time_us=startup_time_us, completion_time_us=completion_time_us)

        return MeasurementRunResult(measurement_run, metrics)

    def get_module_name(self, measurement_run):
        return "fib-iter.wasm" if measurement_run.measurement_spec.workload_type == WorkloadType.FIB_ITERATIVE else "fib-recu.wasm"

    def get_startup_time(self, completed_process, startup_before_utc):
        startup_str = completed_process.stdout.decode('utf-8').split("\n")[1][0:27]  # select timestamp from stdout
        tsp = datetime.datetime.strptime(startup_str, "%Y-%m-%dT%H:%M:%S.%f%z")
        startup_time_diff = tsp - startup_before_utc
        startup_time_us = math.floor(startup_time_diff.total_seconds() * 1000 * 1000)  # from seconds to microseconds
        return startup_time_us
