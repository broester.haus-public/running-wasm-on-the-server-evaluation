import math
import time
from subprocess import CompletedProcess

from domain import *
from helper import subprocess_helper, requests_helper
from platform_executors.abstract_executor import AbstractExecutor


class WasmcloudExecutor(AbstractExecutor):

    def run_http(self, measurement_run: MeasurementRun) -> MeasurementRunResult:
        metrics = requests_helper.start_fortio_load(self.platform_config, self.env, measurement_run.measurement_spec.workload_size)
        return MeasurementRunResult(measurement_run, ResultMetrics(http_metrics=metrics))

    def run_non_http(self, measurement_run: MeasurementRun) -> MeasurementRunResult:
        oci_name = self.get_oci_name(measurement_run)
        fibonacci_n = self.get_fibonacci_n(measurement_run)
        cluster_seed = self.get_cluster_seed()

        ns_before = time.perf_counter_ns()
        ns_before_actor = time.perf_counter_ns()

        host_id = self.determine_host_id(timeout=30)  # determining host id is part of pre-startup
        print("host id " + str(host_id))
        self.start_actor(host_id, oci_name)

        ns_after_actor = time.perf_counter_ns()

        actor_id = self.determine_actor_id(host_id)
        print("actor id " + str(actor_id))
        subprocess_helper.execute_subprocess_unwatched([self.platform_config.run_script, cluster_seed, actor_id,
                                                        fibonacci_n], timeout=360)

        ns_before_stop = time.perf_counter_ns()
        self.stop_actor(actor_id, host_id)
        ns_after_stop = time.perf_counter_ns()

        ns_after = time.perf_counter_ns()

        startup_time_us = math.floor((ns_after_actor - ns_before_actor) / 1000)
        stop_time_us = math.floor((ns_after_stop - ns_before_stop) / 1000)
        completion_time_us = math.floor((ns_after - ns_before) / 1000)

        metrics = ResultMetrics(startup_time_us=startup_time_us, completion_time_us=completion_time_us, stop_time_us=stop_time_us)

        return MeasurementRunResult(measurement_run, metrics)

    def get_cluster_seed(self):
        return subprocess_helper.execute_subprocess(
            [self.platform_config.auxiliary_script_path + "/get-cluster-seed.sh"]).stdout

    def get_oci_name(self, measurement_run):
        return "wasmcloud-fib-recursive:1" \
            if measurement_run.measurement_spec.workload_type == WorkloadType.FIB_RECURSIVE \
            else "wasmcloud-fib-iterative:1"

    def stop_actor(self, actor_id, host_id):
        p = subprocess_helper.execute_subprocess(
            [self.platform_config.auxiliary_script_path + "/post-run.sh", host_id, actor_id])
        print(p.stdout)

    def start_actor(self, host_id, oci_name):
        p = subprocess_helper.execute_subprocess(
            [self.platform_config.auxiliary_script_path + "/pre-run.sh", host_id, oci_name])
        print(p.stdout)

    def determine_actor_id(self, host_id):
        return subprocess_helper.execute_subprocess(
            [self.platform_config.auxiliary_script_path + "/get-actor-id.sh", host_id]).stdout

    def determine_host_id(self, timeout):
        return subprocess_helper.execute_subprocess(
            [self.platform_config.auxiliary_script_path + "/get-host-id.sh"], timeout=timeout).stdout

    def startup(self) -> CompletedProcess:
        process = subprocess_helper.execute_subprocess_unwatched([self.platform_config.startup_script])

        for i in range(20):
            is_up = self.determine_host_id(timeout=10)
            print("Is up? " + str(is_up))
            if len(str(is_up)) > 30:
                break
            time.sleep(1)

        return process

    def open_http(self) -> None:
        subprocess_helper.execute_subprocess_unwatched(
            [self.platform_config.http_up_script, self.determine_host_id(timeout=45)])
        pass

    def close_http(self) -> None:
        subprocess_helper.execute_subprocess_unwatched(
            [self.platform_config.http_down_script, self.determine_host_id(timeout=60)])
        pass

    def shutdown(self) -> None:
        subprocess_helper.execute_subprocess_unwatched([self.platform_config.shutdown_script])
