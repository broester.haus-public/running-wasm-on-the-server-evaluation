from subprocess import CompletedProcess

from config import PlatformConfig, Env
from domain import *
from helper import workload_size_helper


class AbstractExecutor:

    def run(self, measurement_run: MeasurementRun) -> MeasurementRunResult:
        if measurement_run.measurement_spec.workload_type == WorkloadType.HTTP:
            return self.run_http(measurement_run)
        else:
            return self.run_non_http(measurement_run)

    def run_http(self, measurement_run: MeasurementRun) -> MeasurementRunResult:
        pass

    def run_non_http(self, measurement_run: MeasurementRun) -> MeasurementRunResult:
        pass

    def startup(self) -> None | CompletedProcess:
        pass

    def open_http(self) -> None:
        pass

    def close_http(self) -> None:
        pass

    def shutdown(self) -> None:
        pass

    def get_path_to_module_by_measurement_run(self, measurement_run: MeasurementRun) -> str:
        return self.get_path_to_module(
            measurement_run.measurement_spec.platform,
            measurement_run.measurement_spec.workload_type)

    def get_path_to_module(self, platform: Platform, workload_type: WorkloadType) -> str:
        return str.format("{}/{}/{}/module.wasm", self.env.wasm_module_base_path,
                          platform.name,
                          workload_type.name)

    def get_file_in_workload_dir(self, platform: Platform, workload_type: WorkloadType, filename: str) -> str:
        return str.format("{}/{}/{}/{}", self.env.wasm_module_base_path,
                          platform.name,
                          workload_type.name,
                          filename)

    def get_fibonacci_n(self, measurement_run: MeasurementRun) -> str:
        return str(workload_size_helper.get_fibonacci_n(measurement_run.measurement_spec.workload_type,
                                                        measurement_run.measurement_spec.workload_size))

    def __init__(self, platform_config: PlatformConfig, env: Env):
        self.env = env
        self.platform_config = platform_config
