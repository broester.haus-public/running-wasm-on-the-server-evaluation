#!/bin/bash

cd $(dirname $0)

make

mkdir -p ../../../../target/WASMCLOUD/FIB_ITERATIVE
cp build/fibi_s.wasm ../../../../target/WASMCLOUD/FIB_ITERATIVE/module.wasm

wash reg push localhost:5000/wasmcloud-fib-iterative:1 build/fibi_s.wasm --insecure