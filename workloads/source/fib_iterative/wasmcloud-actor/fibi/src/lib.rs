use wasmbus_rpc::actor::prelude::*;
use wasmcloud_interface_httpserver::{HttpRequest, HttpResponse, HttpServer, HttpServerReceiver};

#[derive(Debug, Default, Actor, HealthResponder)]
#[services(Actor, HttpServer)]
struct FibiActor {}

/// Implementation of HttpServer trait methods
#[async_trait]
impl HttpServer for FibiActor {

    async fn handle_request(
        &self,
        _ctx: &Context,
        req: &HttpRequest,
    ) -> std::result::Result<HttpResponse, RpcError> {
        let n = form_urlencoded::parse(req.query_string.as_bytes())
            .find(|(k, _)| k == "n")
            .map(|(_, v)| v.to_string())
            .unwrap();

        let result = FibiActor::calc_fib(n.parse().unwrap());

        Ok(HttpResponse {
            body: i64::to_ne_bytes(result).to_vec(),
            ..Default::default()
        })
    }
}

impl FibiActor {
    fn calc_fib(n:i64) -> i64 {
        let mut first_number:i64;
        let mut second_number:i64 = 0;
        let mut current_number:i64 = 1;
        
        let mut i:i64 = 1;
        
        while i < n {
            first_number = second_number;
            second_number = current_number;
            current_number = first_number + second_number;
            i += 1;
        }
        return current_number
        }
}
