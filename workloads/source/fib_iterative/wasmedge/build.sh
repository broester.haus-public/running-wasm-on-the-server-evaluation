#!/bin/bash

cd $(dirname $0)
cp ../../../target/CLI_WASMER/FIB_ITERATIVE/module.wasm ./module.wasm
chmod +x ./module.wasm

sudo buildah build --annotation "module.wasm.image/variant=compat" -t wasmedge-fib-iterative . 
sudo buildah push --tls-verify=false wasmedge-fib-iterative docker://localhost:5000/wasmedge-fib-iterative:1