#!/bin/bash

PATH=$PATH:../../../../platforms/vino/vino
make
mkdir -p ../../../target/VINO/FIB_ITERATIVE
cp ./build/fib_s.wasm ../../../target/VINO/FIB_ITERATIVE/module.wasm