pub use crate::components::generated::cmp::*;

pub(crate) fn job(input: Inputs, output: OutputPorts) -> JobResult {
    let result = fib(input.input);
    output.output.done(&result)?;
    Ok(())
}

fn fib(n:i64) -> i64 {
    let mut first_number:i64;
    let mut second_number:i64 = 0;
    let mut current_number:i64 = 1;
 
    let mut i:i64 = 1;
 
    while i < n {
 
        first_number = second_number;
 
        second_number = current_number;
 
        current_number = first_number + second_number;
 
        i += 1;
    }
    return current_number
}