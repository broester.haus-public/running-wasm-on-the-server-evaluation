use std::time::{SystemTime, UNIX_EPOCH};

fn main() {
    let start = SystemTime::now();
    let since_the_epoch = start
        .duration_since(UNIX_EPOCH)
        .expect("Time error");
    println!("#Startup_complete {:?}", since_the_epoch);
    calc_fib(std::env::args().nth(1).expect("No n given").parse().unwrap());
}

fn calc_fib(n:i64) -> i64 {
    let mut first_number:i64;
    let mut second_number:i64 = 0;
    let mut current_number:i64 = 1;
 
    let mut i:i64 = 1;
 
    while i < n {
 
        first_number = second_number;
 
        second_number = current_number;
 
        current_number = first_number + second_number;
 
        i += 1;
    }
    return current_number
}