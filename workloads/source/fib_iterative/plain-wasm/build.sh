#!/bin/bash

cargo wasi build --release
mkdir -p ../../../target/CLI_WASMER/FIB_ITERATIVE
mkdir -p ../../../target/CLI_WASMTIME/FIB_ITERATIVE
cp ./target/wasm32-wasi/release/plain-wasm.wasm ../../../target/CLI_WASMER/FIB_ITERATIVE/module.wasm
cp ./target/wasm32-wasi/release/plain-wasm.wasm ../../../target/CLI_WASMTIME/FIB_ITERATIVE/module.wasm