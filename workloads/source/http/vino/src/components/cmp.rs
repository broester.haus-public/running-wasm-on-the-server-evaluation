pub use crate::components::generated::cmp::*;

pub(crate) fn job(input: Inputs, output: OutputPorts) -> JobResult {
    output.echo.done(&input.input)?;
    Ok(())
}
