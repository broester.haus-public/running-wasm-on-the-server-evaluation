#!/bin/bash

cd $(dirname $0)

cargo wasi build --release

cp ./target/wasm32-wasi/release/http_server.wasm ./module.wasm
chmod +x ./module.wasm
mkdir -p ../../../../target/WASMEDGE_ON_K8S/HTTP
cp ./module.wasm ../../../../target/WASMEDGE_ON_K8S/HTTP/module.wasm

sudo buildah build --annotation "module.wasm.image/variant=compat" -t wasmedge-http-echo . 
sudo buildah push --tls-verify=false wasmedge-http-echo docker://localhost:5000/wasmedge-http-echo:1