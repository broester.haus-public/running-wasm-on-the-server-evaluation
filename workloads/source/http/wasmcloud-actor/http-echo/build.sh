#!/bin/bash

cd $(dirname $0)

make

mkdir -p ../../../../target/WASMCLOUD/HTTP
cp build/http_echo_s.wasm ../../../../target/WASMCLOUD/HTTP/module.wasm

wash reg push localhost:5000/wasmcloud-http-echo:1 build/http_echo_s.wasm --insecure