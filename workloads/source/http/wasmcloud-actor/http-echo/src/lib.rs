use wasmbus_rpc::actor::prelude::*;
use wasmcloud_interface_httpserver::{HttpRequest, HttpResponse, HttpServer, HttpServerReceiver};

#[derive(Debug, Default, Actor, HealthResponder)]
#[services(Actor, HttpServer)]
struct HttpEchoActor {}

#[async_trait]
impl HttpServer for HttpEchoActor {

    async fn handle_request(
        &self,
        _ctx: &Context,
        req: &HttpRequest,
    ) -> std::result::Result<HttpResponse, RpcError> {
        let body = &req.body;

        Ok(HttpResponse {
            body: body.to_vec(),
            ..Default::default()
        })
    }
}

