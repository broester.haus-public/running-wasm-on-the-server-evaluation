#!/bin/bash
cd $(dirname $0)
cp ../../../target/CLI_WASMTIME/FIB_ITERATIVE/module.wasm ./fib-iter.wasm
cp ../../../target/CLI_WASMTIME/FIB_RECURSIVE/module.wasm ./fib-recu.wasm

docker build -t wasmtime-fib .
docker image tag wasmtime-fib:latest localhost:5000/wasmtime-fib:1
docker push localhost:5000/wasmtime-fib:1