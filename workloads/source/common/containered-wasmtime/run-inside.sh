#!/bin/bash

echo "Docker init complete" | ts %Y-%m-%dT%H:%M:%.SZ
~/.wasmtime/bin/wasmtime run $1 -- $2 | ts %Y-%m-%dT%H:%M:%.SZ
echo "wasmtime complete" | ts %Y-%m-%dT%H:%M:%.SZ