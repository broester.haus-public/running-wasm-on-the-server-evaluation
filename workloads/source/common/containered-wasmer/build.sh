#!/bin/bash
cd $(dirname $0)
cp ../../../target/CLI_WASMER/FIB_ITERATIVE/module.wasm ./fib-iter.wasm
cp ../../../target/CLI_WASMER/FIB_RECURSIVE/module.wasm ./fib-recu.wasm

docker build -t wasmer-fib .
docker image tag wasmer-fib:latest localhost:5000/wasmer-fib:1
docker push localhost:5000/wasmer-fib:1