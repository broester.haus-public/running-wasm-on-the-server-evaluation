#!/bin/bash

echo "Docker init complete" | ts %Y-%m-%dT%H:%M:%.SZ
wasmer run $1 -- $2 | ts %Y-%m-%dT%H:%M:%.SZ
echo "wasmer complete" | ts %Y-%m-%dT%H:%M:%.SZ