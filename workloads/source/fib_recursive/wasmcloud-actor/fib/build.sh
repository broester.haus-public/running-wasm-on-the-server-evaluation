#!/bin/bash

cd $(dirname $0)

make

mkdir -p ../../../../target/WASMCLOUD/FIB_RECURSIVE
cp build/fib_s.wasm ../../../../target/WASMCLOUD/FIB_RECURSIVE/module.wasm

wash reg push localhost:5000/wasmcloud-fib-recursive:1 build/fib_s.wasm --insecure