use wasmbus_rpc::actor::prelude::*;
use wasmcloud_interface_httpserver::{HttpRequest, HttpResponse, HttpServer, HttpServerReceiver};

#[derive(Debug, Default, Actor, HealthResponder)]
#[services(Actor, HttpServer)]
struct FibActor {}

/// Implementation of HttpServer trait methods
#[async_trait]
impl HttpServer for FibActor {

    async fn handle_request(
        &self,
        _ctx: &Context,
        req: &HttpRequest,
    ) -> std::result::Result<HttpResponse, RpcError> {
        let n = form_urlencoded::parse(req.query_string.as_bytes())
            .find(|(k, _)| k == "n")
            .map(|(_, v)| v.to_string())
            .unwrap();

        let result = FibActor::calc_fib(n.parse().unwrap());

        Ok(HttpResponse {
            body: i64::to_ne_bytes(result).to_vec(),
            ..Default::default()
        })
    }
}

impl FibActor {
    fn calc_fib(n:i64) -> i64 {
        if n <= 1 {
            return n;
        } else {
            return FibActor::calc_fib(n-1) + FibActor::calc_fib(n-2)
        }
    }
}