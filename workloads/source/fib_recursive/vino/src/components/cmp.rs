pub use crate::components::generated::cmp::*;

pub(crate) fn job(input: Inputs, output: OutputPorts) -> JobResult {
    let result = fib(input.input);
    output.output.done(&result)?;
    Ok(())
}

fn fib(n:i64) -> i64 {
    if n <= 1 {
        return n;
    } else {
        return fib(n-1) + fib(n-2)
    }
}