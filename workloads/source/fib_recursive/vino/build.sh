#!/bin/bash

PATH=$PATH:../../../../platforms/vino/vino
make
mkdir -p ../../../target/VINO/FIB_RECURSIVE
cp ./build/fib_s.wasm ../../../target/VINO/FIB_RECURSIVE/module.wasm