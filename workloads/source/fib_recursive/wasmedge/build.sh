#!/bin/bash

cd $(dirname $0)
cp ../../../target/CLI_WASMER/FIB_RECURSIVE/module.wasm ./module.wasm
chmod +x ./module.wasm

sudo buildah build --annotation "module.wasm.image/variant=compat" -t wasmedge-fib-recursive . 
sudo buildah push --tls-verify=false wasmedge-fib-recursive docker://localhost:5000/wasmedge-fib-recursive:1