#!/bin/bash

cd $(dirname $0)
cp ../../../target/CLI_WASMER/FIB_RECURSIVE/module.wasm ./module.wasm
chmod +x ./module.wasm

wasm-to-oci push ./module.wasm localhost:5000/krustlet-fib-recursive:2