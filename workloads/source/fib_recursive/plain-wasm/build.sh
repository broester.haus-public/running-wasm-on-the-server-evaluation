#!/bin/bash

cargo wasi build --release
mkdir -p ../../../target/CLI_WASMER/FIB_RECURSIVE
mkdir -p ../../../target/CLI_WASMTIME/FIB_RECURSIVE
cp ./target/wasm32-wasi/release/plain-wasm.wasm ../../../target/CLI_WASMER/FIB_RECURSIVE/module.wasm
cp ./target/wasm32-wasi/release/plain-wasm.wasm ../../../target/CLI_WASMTIME/FIB_RECURSIVE/module.wasm