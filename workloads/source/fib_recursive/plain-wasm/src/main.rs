use std::time::{SystemTime, UNIX_EPOCH};

fn main() {
    let start = SystemTime::now();
    let since_the_epoch = start
        .duration_since(UNIX_EPOCH)
        .expect("Time error");
    println!("#Startup_complete {:?}", since_the_epoch);
    calc_fib(std::env::args().nth(1).expect("No n given").parse().unwrap());
}

fn calc_fib(n:i64) -> i64 {
    if n <= 1 {
        return n;
    } else {
        return calc_fib(n-1) + calc_fib(n-2)
    }
}