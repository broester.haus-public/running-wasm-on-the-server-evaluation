#!/bin/bash

# $1 location of the fortio server 
# $2 some id for result identification

EXPORTFILENAME="fortio-result-${2}.json"
curl -d @fortio-cfg.json "${1}/fortio/rest/run?jsonPath=.metadata" --no-progress-meter > $EXPORTFILENAME && echo "Done. See $EXPORTFILENAME"