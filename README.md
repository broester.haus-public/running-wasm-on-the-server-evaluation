# Performance Evaluation of Platforms and Technologies for Running WebAssembly on the Server

# Directory Structure

- [`analysis`](analysis): Results of the performed experiments and their evaluation using Jupyter Notebook. Also: generated svg diagrams
- [`measurement-tool`](measurement-tool): Python-based measurement tool, used for automated execution of experiments 
- [`platforms`](platforms): Shell scripts for installing, starting up, running workloads, and shutting down platforms/technologies
- [`workloads`](workloads): Workloads for the platforms/technologies.

# Running the evaluation

Prerequisites: Rust, Cargo, Go, Docker (running), Python 3.10

- Install all platforms using the script [`sudo platforms/install-all.sh`](platforms/install-all.sh)
- Configure the measurement requests and the environment in [`config.yaml`](config.yaml)
- Create a python virtual env (venv) under `measurement-tool/venv/` and install the following packages: PyYAML, requests, munch, jq
   ```
    python3 -m venv measurement-tool/venv
    measurement-tool/venv/bin/activate

    python3 -m pip install PyYAML munch jq requests
   ```
- If runs of workload `http-echo` are requested, start fortio (on another machine): `docker run -d --network="host" --restart=always --name fortio fortio/fortio server`
- Run [`eval.sh`](eval.sh) to execute the requested measurements and see the results in the directory `results`
- Analyse the collected metrics using the provided Jupyter Notebook in the directory `analysis`

### Sidenotes
 - Installation of platforms and startup of platforms requires entering your password for sudo. As this may happen within the measurement, it can be helpful to temporarily remove sudo password prompting: https://askubuntu.com/questions/147241/execute-sudo-without-password
 - As the installations of the platforms are highly dependant on concrete library versions installed on your system, the installation may fail unexpectedly. Please refer to logs and the platforms documentation in that case.