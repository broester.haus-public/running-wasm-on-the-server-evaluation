# For analysis the package manager `mamba` is used

1. `mamba create -n analysis`
2. `mamba activate analysis`
3. Install required packages `mamba install xyz`
4. `mamba run jupyter-lab`